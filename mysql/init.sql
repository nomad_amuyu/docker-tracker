-- --------------------------------------------------------
-- 호스트:                          tracker-dev-aurora2.caxwpgabie9v.ap-northeast-2.rds.amazonaws.com
-- 서버 버전:                        5.6.10 - MySQL Community Server (GPL)
-- 서버 OS:                        Linux
-- HeidiSQL 버전:                  9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- icon_dev 데이터베이스 구조 내보내기
CREATE DATABASE IF NOT EXISTS `icon_dev` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `icon_dev`;


-- 테이블 icon_dev.T_ACCESS_LOG 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_ACCESS_LOG` (
  `ac_index` int(9) NOT NULL AUTO_INCREMENT,
  `server_name` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT 'server name',
  `api` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT 'call api // 호출된 api ',
  `count` int(11) DEFAULT NULL COMMENT 'count // 카운트 ',
  `create_date` timestamp NULL DEFAULT NULL COMMENT 'connect date // 접속 날짜 ',
  `end_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ac_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='API 접속 카운트용';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_ADDRESS 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_ADDRESS` (
  `address` varchar(68) NOT NULL COMMENT 'address / 주소',
  `balance` varchar(32) DEFAULT NULL COMMENT 'icx balance / 잔액',
  `tx_count` int(11) DEFAULT NULL COMMENT 'transaction count  / 트랜잭션 개수',
  `node_type` varchar(10) DEFAULT NULL COMMENT 'node_type  / 노드 타입',
  PRIMARY KEY (`address`),
  UNIQUE KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='top Icx Holder list / 상위 ICX 홀더 리스트 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_ADDRESS_TOTAL 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_ADDRESS_TOTAL` (
  `address` varchar(68) NOT NULL COMMENT 'address   / 주소',
  `balance` varchar(32) DEFAULT NULL COMMENT 'balance  / 잔액',
  `tx_count` int(11) DEFAULT NULL COMMENT 'transaction count  / 트랜잭션 개수',
  `node_type` varchar(10) DEFAULT NULL COMMENT 'node type   /  노드 타입',
  `is_update` tinyint(1) DEFAULT '0' COMMENT '1 블록 처리 시 업데이트 여부',
  PRIMARY KEY (`address`),
  UNIQUE KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Total user address / 모든 유저 주소 정보 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_BLOCK 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_BLOCK` (
  `hash` varchar(68) DEFAULT NULL COMMENT '블록 해시',
  `height` int(11) NOT NULL COMMENT '블록 높이',
  `create_date` timestamp NULL DEFAULT NULL COMMENT '생성 시간',
  `c_rep` varchar(68) DEFAULT NULL COMMENT 'C-rep',
  `tx_count` int(11) DEFAULT NULL COMMENT 'tx 개수',
  `prev_hash` varchar(68) DEFAULT NULL COMMENT '이전 해시',
  `block_size` int(11) DEFAULT NULL COMMENT '블록사이즈',
  `fee` varchar(30) DEFAULT NULL COMMENT '세금',
  `amount` varchar(30) DEFAULT NULL COMMENT '금액',
  PRIMARY KEY (`height`),
  UNIQUE KEY `height_UNIQUE` (`height`),
  KEY `height` (`height`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Recenty Block / 최근 블록 정보 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_BLOCK_TOTAL 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_BLOCK_TOTAL` (
  `hash` varchar(68) DEFAULT NULL COMMENT '블록 해시',
  `height` int(11) NOT NULL COMMENT '블록 높이',
  `create_date` timestamp NULL DEFAULT NULL COMMENT '생성 시간',
  `c_rep` varchar(68) DEFAULT NULL COMMENT 'C-rep',
  `tx_count` int(11) DEFAULT NULL COMMENT 'tx 개수',
  `prev_hash` varchar(68) DEFAULT NULL COMMENT '이전 해시',
  `block_size` int(11) DEFAULT NULL COMMENT '블록사이즈',
  `fee` varchar(30) DEFAULT NULL COMMENT '세금',
  `amount` varchar(30) DEFAULT NULL COMMENT '금액',
  PRIMARY KEY (`height`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='total  Block / 모든 블록 정보 '
/*!50100 PARTITION BY RANGE (height)
(PARTITION p00 VALUES LESS THAN (1000000) ENGINE = InnoDB,
 PARTITION p01 VALUES LESS THAN (2000000) ENGINE = InnoDB,
 PARTITION p02 VALUES LESS THAN (3000000) ENGINE = InnoDB,
 PARTITION p03 VALUES LESS THAN (4000000) ENGINE = InnoDB,
 PARTITION p04 VALUES LESS THAN (5000000) ENGINE = InnoDB,
 PARTITION p05 VALUES LESS THAN (6000000) ENGINE = InnoDB,
 PARTITION p06 VALUES LESS THAN (7000000) ENGINE = InnoDB,
 PARTITION p07 VALUES LESS THAN (8000000) ENGINE = InnoDB,
 PARTITION p08 VALUES LESS THAN (9000000) ENGINE = InnoDB,
 PARTITION p09 VALUES LESS THAN (10000000) ENGINE = InnoDB,
 PARTITION p10 VALUES LESS THAN (11000000) ENGINE = InnoDB,
 PARTITION p11 VALUES LESS THAN (12000000) ENGINE = InnoDB,
 PARTITION p12 VALUES LESS THAN (13000000) ENGINE = InnoDB,
 PARTITION p13 VALUES LESS THAN (14000000) ENGINE = InnoDB,
 PARTITION p14 VALUES LESS THAN (15000000) ENGINE = InnoDB,
 PARTITION p15 VALUES LESS THAN (16000000) ENGINE = InnoDB,
 PARTITION p16 VALUES LESS THAN (17000000) ENGINE = InnoDB,
 PARTITION p17 VALUES LESS THAN (18000000) ENGINE = InnoDB,
 PARTITION p18 VALUES LESS THAN (19000000) ENGINE = InnoDB,
 PARTITION p19 VALUES LESS THAN (20000000) ENGINE = InnoDB,
 PARTITION p20 VALUES LESS THAN (21000000) ENGINE = InnoDB,
 PARTITION p21 VALUES LESS THAN (22000000) ENGINE = InnoDB,
 PARTITION p22 VALUES LESS THAN (23000000) ENGINE = InnoDB,
 PARTITION p23 VALUES LESS THAN (24000000) ENGINE = InnoDB,
 PARTITION p24 VALUES LESS THAN (25000000) ENGINE = InnoDB,
 PARTITION p25 VALUES LESS THAN (26000000) ENGINE = InnoDB,
 PARTITION p26 VALUES LESS THAN (27000000) ENGINE = InnoDB,
 PARTITION p27 VALUES LESS THAN (28000000) ENGINE = InnoDB,
 PARTITION p28 VALUES LESS THAN (29000000) ENGINE = InnoDB,
 PARTITION p29 VALUES LESS THAN (30000000) ENGINE = InnoDB,
 PARTITION p30 VALUES LESS THAN (31000000) ENGINE = InnoDB,
 PARTITION p31 VALUES LESS THAN (32000000) ENGINE = InnoDB,
 PARTITION p32 VALUES LESS THAN (33000000) ENGINE = InnoDB,
 PARTITION p33 VALUES LESS THAN (34000000) ENGINE = InnoDB,
 PARTITION p34 VALUES LESS THAN (35000000) ENGINE = InnoDB,
 PARTITION p35 VALUES LESS THAN (36000000) ENGINE = InnoDB,
 PARTITION p36 VALUES LESS THAN (37000000) ENGINE = InnoDB,
 PARTITION p37 VALUES LESS THAN (38000000) ENGINE = InnoDB,
 PARTITION p38 VALUES LESS THAN (39000000) ENGINE = InnoDB,
 PARTITION p39 VALUES LESS THAN (40000000) ENGINE = InnoDB,
 PARTITION p40 VALUES LESS THAN (41000000) ENGINE = InnoDB,
 PARTITION p41 VALUES LESS THAN (42000000) ENGINE = InnoDB,
 PARTITION p42 VALUES LESS THAN (43000000) ENGINE = InnoDB,
 PARTITION p43 VALUES LESS THAN (44000000) ENGINE = InnoDB,
 PARTITION p44 VALUES LESS THAN (45000000) ENGINE = InnoDB,
 PARTITION p45 VALUES LESS THAN (46000000) ENGINE = InnoDB,
 PARTITION p46 VALUES LESS THAN (47000000) ENGINE = InnoDB,
 PARTITION p47 VALUES LESS THAN (48000000) ENGINE = InnoDB,
 PARTITION p48 VALUES LESS THAN (49000000) ENGINE = InnoDB,
 PARTITION p49 VALUES LESS THAN (50000000) ENGINE = InnoDB,
 PARTITION p50 VALUES LESS THAN (51000000) ENGINE = InnoDB,
 PARTITION p51 VALUES LESS THAN (52000000) ENGINE = InnoDB,
 PARTITION p52 VALUES LESS THAN (53000000) ENGINE = InnoDB,
 PARTITION p53 VALUES LESS THAN (54000000) ENGINE = InnoDB,
 PARTITION p54 VALUES LESS THAN (55000000) ENGINE = InnoDB,
 PARTITION p55 VALUES LESS THAN (56000000) ENGINE = InnoDB,
 PARTITION p56 VALUES LESS THAN (57000000) ENGINE = InnoDB,
 PARTITION p57 VALUES LESS THAN (58000000) ENGINE = InnoDB,
 PARTITION p58 VALUES LESS THAN (59000000) ENGINE = InnoDB,
 PARTITION p59 VALUES LESS THAN (60000000) ENGINE = InnoDB,
 PARTITION p60 VALUES LESS THAN (61000000) ENGINE = InnoDB,
 PARTITION p61 VALUES LESS THAN (62000000) ENGINE = InnoDB,
 PARTITION p62 VALUES LESS THAN (63000000) ENGINE = InnoDB,
 PARTITION p63 VALUES LESS THAN (64000000) ENGINE = InnoDB,
 PARTITION p64 VALUES LESS THAN (65000000) ENGINE = InnoDB,
 PARTITION p65 VALUES LESS THAN (66000000) ENGINE = InnoDB,
 PARTITION p66 VALUES LESS THAN (67000000) ENGINE = InnoDB,
 PARTITION p67 VALUES LESS THAN (68000000) ENGINE = InnoDB,
 PARTITION p68 VALUES LESS THAN (69000000) ENGINE = InnoDB,
 PARTITION p69 VALUES LESS THAN (70000000) ENGINE = InnoDB,
 PARTITION p70 VALUES LESS THAN (71000000) ENGINE = InnoDB,
 PARTITION p71 VALUES LESS THAN (72000000) ENGINE = InnoDB,
 PARTITION p72 VALUES LESS THAN (73000000) ENGINE = InnoDB,
 PARTITION p73 VALUES LESS THAN (74000000) ENGINE = InnoDB,
 PARTITION p74 VALUES LESS THAN (75000000) ENGINE = InnoDB,
 PARTITION p75 VALUES LESS THAN (76000000) ENGINE = InnoDB,
 PARTITION p76 VALUES LESS THAN (77000000) ENGINE = InnoDB,
 PARTITION p77 VALUES LESS THAN (78000000) ENGINE = InnoDB,
 PARTITION p78 VALUES LESS THAN (79000000) ENGINE = InnoDB,
 PARTITION p79 VALUES LESS THAN (80000000) ENGINE = InnoDB,
 PARTITION p80 VALUES LESS THAN (81000000) ENGINE = InnoDB,
 PARTITION p81 VALUES LESS THAN (82000000) ENGINE = InnoDB,
 PARTITION p82 VALUES LESS THAN (83000000) ENGINE = InnoDB,
 PARTITION p83 VALUES LESS THAN (84000000) ENGINE = InnoDB,
 PARTITION p84 VALUES LESS THAN (85000000) ENGINE = InnoDB,
 PARTITION p85 VALUES LESS THAN (86000000) ENGINE = InnoDB,
 PARTITION p86 VALUES LESS THAN (87000000) ENGINE = InnoDB,
 PARTITION p87 VALUES LESS THAN (88000000) ENGINE = InnoDB,
 PARTITION p88 VALUES LESS THAN (89000000) ENGINE = InnoDB,
 PARTITION p89 VALUES LESS THAN (90000000) ENGINE = InnoDB,
 PARTITION p90 VALUES LESS THAN (91000000) ENGINE = InnoDB,
 PARTITION p91 VALUES LESS THAN (92000000) ENGINE = InnoDB,
 PARTITION p92 VALUES LESS THAN (93000000) ENGINE = InnoDB,
 PARTITION p93 VALUES LESS THAN (94000000) ENGINE = InnoDB,
 PARTITION p94 VALUES LESS THAN (95000000) ENGINE = InnoDB,
 PARTITION p95 VALUES LESS THAN (96000000) ENGINE = InnoDB,
 PARTITION p96 VALUES LESS THAN (97000000) ENGINE = InnoDB,
 PARTITION p97 VALUES LESS THAN (98000000) ENGINE = InnoDB,
 PARTITION p98 VALUES LESS THAN (99000000) ENGINE = InnoDB,
 PARTITION p99 VALUES LESS THAN (100000000) ENGINE = InnoDB,
 PARTITION p100 VALUES LESS THAN MAXVALUE ENGINE = InnoDB) */;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_CONFIG 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_CONFIG` (
  `scheduler_version` varchar(8) COLLATE utf8_bin NOT NULL COMMENT '스케쥴러 버젼 /  v2 : v3',
  `scheduler_name` varchar(8) COLLATE utf8_bin NOT NULL COMMENT 'scheduler name  //  스케쥴러 이름 ',
  `limit_block` int(11) DEFAULT NULL COMMENT 'Size of T_Block     //  T_Block 에 들어갈 블록 사이즈 ',
  `limit_tx` int(11) DEFAULT NULL COMMENT 'Size of T_Transaction    //  T_Transaction 에 들어갈 사이즈 ',
  `limit_address` int(11) DEFAULT NULL COMMENT 'Size of T_Address   //  T_Address 에 들어갈 사이즈 ',
  `limit_main_block` int(11) DEFAULT NULL COMMENT 'Size of T_Main_Block  // T_Main_Block 의 사이즈 ',
  `limit_main_tx` int(11) DEFAULT NULL COMMENT 'Size of T_Main_tx  // T_Main_tx 의 사이즈 ',
  PRIMARY KEY (`scheduler_version`,`scheduler_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tracker System Config / 트래커 시스템 설정 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_CONTRACT 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_CONTRACT` (
  `contract_addr` varchar(68) COLLATE utf8_bin NOT NULL COMMENT '주소',
  `version` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `irc_version` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `state` int(11) DEFAULT NULL COMMENT '0: 대기 1: 성공 2: 실패',
  PRIMARY KEY (`contract_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_CONTRACT_HISTORY 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_CONTRACT_HISTORY` (
  `contract_addr` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'contract address / 컨트렉트 주소 ',
  `version` int(11) NOT NULL COMMENT 'contract version / 컨트렉트 버젼 : v2  v3 ',
  `compiler` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT 'compier version( to_do  ) /  컴파일러 버젼 (추후 개발 예정 )',
  `create_tx` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'create request transaction / 생성 요청 트랜잭션 ',
  `create_date` timestamp NULL DEFAULT NULL COMMENT 'create request date / 생성 요청 시간 ',
  `verified_tx` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'score verified transaction hash / 검증 tx hash ',
  `verified_date` timestamp NULL DEFAULT NULL COMMENT 'score verified date / 검증 날짜 ',
  `creator` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'score creator address /  스코어 생성자 주소 ',
  `state` int(11) DEFAULT NULL COMMENT 'score status  (0 : pendding / 1: accept / 2: rejected   ) / 스코어 상태값  (0 : pendding / 1: accept / 2: rejected   )',
  PRIMARY KEY (`contract_addr`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='contract history info //  컨트랙트  이력 정보 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_CONTRACT_TX 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_CONTRACT_TX` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_hash` varchar(68) COLLATE utf8_bin NOT NULL,
  `contract_addr` varchar(68) COLLATE utf8_bin NOT NULL COMMENT '주소',
  `age` timestamp NULL DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `from_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `amount` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `to_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL,
  `tx_type` int(11) DEFAULT NULL,
  `fee` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contract_addr` (`contract_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_CONTRACT_TX_OLD 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_CONTRACT_TX_OLD` (
  `tx_hash` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'transaction hash / 트랜잭션 해시 ',
  `contract_addr` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'contract address // 컨트랙트 주소',
  `age` timestamp NULL DEFAULT NULL COMMENT 'tx time / tx 생성 시간 ',
  `height` int(11) DEFAULT NULL,
  `from_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'send address / 보낸 사람 주소 ',
  `state` int(11) DEFAULT NULL COMMENT 'tx status  (0: fail / 1: success) /  트랜 잭션 결과값   (0: fail / 1: success)',
  `amount` varchar(30) COLLATE utf8_bin DEFAULT NULL COMMENT 'icx amount / icx 금액 ',
  `to_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'recieve address / 수신 주소 ',
  `tx_type` int(11) DEFAULT NULL COMMENT 'transaction type  / 트랙잭션 타입 \n\n (0: icx send  1: token send 2: score call 3 : score install  4:  score update\n 5: score accept  6: score reject ) ',
  `fee` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`tx_hash`,`contract_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='contract transaction list / 컨트랙트 관련 Tx list ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_CURRENT_EXCHANGE 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_CURRENT_EXCHANGE` (
  `market_name` varchar(15) NOT NULL COMMENT 'market name / 거래소이름 \n',
  `trade_name` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'trade name / 거래 이름',
  `create_date` timestamp NULL DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `pre_price` varchar(50) DEFAULT NULL,
  `daily_rate` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`market_name`,`trade_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Exchange info / 환율 정보 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_GENESIS 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_GENESIS` (
  `block` varchar(68) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='제네시스 블록의 트랜잭션 메시지를 담기위한 테이블';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_INTERNAL_TX 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_INTERNAL_TX` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_tx_hash` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'parent transaction hash / 부모 트랜잭션 해시 ',
  `tx_index` int(11) NOT NULL COMMENT 'index / 순서 ',
  `contract_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'contract address / 컨트랙트 주소 ',
  `create_date` timestamp NULL DEFAULT NULL COMMENT 'create date / 생성 날짜 ',
  `height` int(11) DEFAULT NULL COMMENT 'block height / 블록 높이 ',
  `from_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'sender address / 보내는 사람 주소 ',
  `to_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'receive address / 수신자 주소 ',
  `amount` varchar(30) COLLATE utf8_bin DEFAULT NULL COMMENT 'icx amount / icx 금액 ',
  `state` int(11) DEFAULT NULL COMMENT 'tx status / tx 결과값   (0 : fail / 1: success)',
  PRIMARY KEY (`id`,`parent_tx_hash`,`tx_index`),
  KEY `contract_addr` (`contract_addr`),
  KEY `height` (`height`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_INTERNAL_TX_VIEW 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_INTERNAL_TX_VIEW` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_tx_hash` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'parent transaction hash / 부모 트랜잭션 해시 ',
  `tx_index` int(11) NOT NULL COMMENT 'index / 순서 ',
  `contract_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'contract address / 컨트랙트 주소 ',
  `create_date` timestamp NULL DEFAULT NULL COMMENT 'create date / 생성 날짜 ',
  `height` int(11) DEFAULT NULL COMMENT 'block height / 블록 높이 ',
  `address` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'address / 보내거나 받은 사람 주소',
  `state` int(11) DEFAULT NULL COMMENT 'tx status / tx 결과값   (0 : fail / 1: success)',
  PRIMARY KEY (`id`,`parent_tx_hash`,`tx_index`),
  KEY `contract_addr` (`contract_addr`),
  KEY `height` (`height`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_MAIN_BLOCK 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_MAIN_BLOCK` (
  `block_height` int(11) NOT NULL COMMENT 'block height / 블록높이  ',
  `hash` varchar(68) DEFAULT NULL COMMENT 'block hash / 블록 해시',
  `crep_addr` varchar(68) DEFAULT NULL COMMENT 'c-rep address / C-rep 주소',
  `tx_count` int(11) DEFAULT NULL COMMENT 'tx count  by block / 트랜잭션 개수',
  `create_date` timestamp NULL DEFAULT NULL COMMENT 'create date / 생성일',
  PRIMARY KEY (`block_height`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='main page block list /  메인 페이지 블록 리스트 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_MAIN_CHART 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_MAIN_CHART` (
  `target_date` date NOT NULL COMMENT 'day / 날짜',
  `tx_count` int(11) DEFAULT NULL COMMENT 'tx count / tx 개수',
  PRIMARY KEY (`target_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Main page dairy chart / 메인 페이지 차트 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_MAIN_INFO 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_MAIN_INFO` (
  `market_cap` varchar(50) NOT NULL COMMENT ' coin market cap /usd 시총 ',
  `icx_supply` varchar(50) DEFAULT NULL COMMENT 'total  supply / 총 공급량',
  `icx_circulationy` varchar(50) DEFAULT NULL COMMENT 'Circulationy / 실제 유통량',
  `transaction_count` int(11) DEFAULT NULL COMMENT 'total icon tx count / 총 트랜잭션 개수',
  `crep_count` int(11) DEFAULT NULL COMMENT 'C-rep count / C-rep 개수',
  `public_treasury` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`market_cap`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='main page info / 메인 페이지 정보 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_MAIN_TX 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_MAIN_TX` (
  `tx_hash` varchar(68) NOT NULL COMMENT '트랜잭션 해쉬',
  `amount` varchar(30) DEFAULT NULL COMMENT '금액',
  `fee` varchar(30) DEFAULT NULL COMMENT '세금',
  `score_yn` varchar(1) DEFAULT NULL COMMENT 'score 여부',
  `create_date` timestamp NULL DEFAULT NULL COMMENT '생성일',
  `state` int(11) DEFAULT NULL COMMENT '0:실패, 1:성공',
  PRIMARY KEY (`tx_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_SCHEDULER_FLAG 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_SCHEDULER_FLAG` (
  `schedule_name` varchar(30) NOT NULL COMMENT 'schedule name / 스케쥴 이름 \n',
  `active_yn` varchar(1) DEFAULT 'N' COMMENT 'active yn  / 동작중 여부',
  `active_server` varchar(30) DEFAULT NULL COMMENT 'active server name / 동작중인 서버 이름',
  `start_position` varchar(68) DEFAULT NULL COMMENT 'schedule start positon  /스케쥴 시작 위치 ',
  `update_date` timestamp NULL DEFAULT NULL COMMENT 'update date / 수정날짜',
  PRIMARY KEY (`schedule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Scheduler Flag / 스케쥴러 관리 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_TOKEN_ADDRESS 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_TOKEN_ADDRESS` (
  `address` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'address / 유저 주소 ',
  `contract_addr` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'token contract address  / 컨트렉트 주소  ',
  `tx_count` int(11) NOT NULL DEFAULT '0' COMMENT 'transaction  count  / 트랜잭션 카운터 ',
  `quantity` varchar(100) COLLATE utf8_bin DEFAULT '0' COMMENT 'token quantity / 토큰 수량 ',
  PRIMARY KEY (`address`,`contract_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='token holder info / 토큰 소유자 정보 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_TOKEN_INFO 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_TOKEN_INFO` (
  `contract_addr` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'contract address /  컨트렉트 주소 ',
  `name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Token Name / 토큰 이름 ',
  `symbol` varchar(8) COLLATE utf8_bin DEFAULT NULL COMMENT 'token symbol / 토큰 심볼 ',
  `holder_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL COMMENT 'token creator address / 토큰 생성자 주소 ',
  `holder_count` int(10) unsigned DEFAULT NULL COMMENT 'token holder count / 토큰 홀더 숫자 ',
  `transfer_count` int(10) unsigned DEFAULT NULL COMMENT 'token transfer count / 토큰 거래 횟수 ',
  `total_supply` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'total_supply / 총 발행량 ',
  `decimals` int(11) DEFAULT NULL COMMENT 'decimals / 자릿수 ',
  PRIMARY KEY (`contract_addr`),
  UNIQUE KEY `contract_addr` (`contract_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='TOKEN INFO / 토큰 기본 정보 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_TOKEN_TX 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_TOKEN_TX` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_addr` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'contract address / 컨트렉트 어드래스 ',
  `tx_hash` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'transaction hash / 트랜잭션 해시 ',
  `tx_index` int(11) NOT NULL COMMENT 'index / 순서 ',
  `age` timestamp NULL DEFAULT NULL COMMENT 'create time / 생성 날짜 ',
  `from_addr` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'sender address / 송금자 주소 ',
  `to_addr` varchar(68) COLLATE utf8_bin NOT NULL COMMENT 'recieve address / 수신자 주소 ',
  `quantity` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'token  quantity / 토큰 수량 ',
  `irc_version` varchar(8) COLLATE utf8_bin DEFAULT NULL COMMENT 'irc version / irc 버젼 ',
  `state` int(11) DEFAULT NULL COMMENT 'tx status / tx 상태값    0: fail , 1:success ',
  `amount` varchar(30) COLLATE utf8_bin DEFAULT NULL COMMENT 'icx amount / icx 금액 ',
  `block_height` int(11) DEFAULT NULL COMMENT 'block height / 블록 높이 ',
  `fee` varchar(30) COLLATE utf8_bin DEFAULT NULL COMMENT 'fee / fee ',
  `tx_type` int(11) DEFAULT NULL COMMENT 'tx_type / 트랜잭션 타입   0: icx send 1: token send  2:contact  call  3 : contract install 4 : contract update 5 : contact reject ',
  PRIMARY KEY (`id`,`contract_addr`),
  KEY `tx_hash` (`tx_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='token transaction / 토큰 트랜잭션 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_TOKEN_TX_VIEW 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_TOKEN_TX_VIEW` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_addr` varchar(68) COLLATE utf8_bin NOT NULL,
  `tx_hash` varchar(68) COLLATE utf8_bin NOT NULL,
  `tx_index` int(11) NOT NULL,
  `address` varchar(68) COLLATE utf8_bin NOT NULL,
  `block_height` int(11) NOT NULL,
  `age` timestamp NOT NULL,
  PRIMARY KEY (`id`,`contract_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='토큰 트랜잭션 검색용 해시 뷰';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_TRANSACTION 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_TRANSACTION` (
  `tx_hash` varchar(68) NOT NULL COMMENT 'transaction hash : 트랜잭션 해시',
  `hash` varchar(68) DEFAULT NULL COMMENT 'block hash / 블록 해시',
  `height` int(11) DEFAULT NULL COMMENT 'block height / 블록 높이',
  `create_date` timestamp NULL DEFAULT NULL COMMENT 'create date / 생성 시간 ',
  `from_addr` varchar(68) DEFAULT NULL COMMENT 'sender address / /송금자 주소',
  `to_addr` varchar(68) DEFAULT NULL COMMENT 'recieve / 수금자 주소',
  `fee` varchar(30) DEFAULT NULL COMMENT 'fee / fee ',
  `amount` varchar(30) DEFAULT NULL COMMENT 'icx amount / 금액',
  `state` int(11) DEFAULT NULL COMMENT 'tx status / 트랜잭션 status  0: fail 1: success ',
  `tx_type` int(11) DEFAULT '0' COMMENT 'tx_type / 트랜잭션 타입   0: icx send 1: token send  2:contact  call  3 : contract install 4 : contract update 5 : contact reject ',
  `step_limit` varchar(32) DEFAULT NULL,
  `step_used_tx` varchar(32) DEFAULT NULL,
  `step_price` varchar(32) DEFAULT NULL,
  `data_type` varchar(10) DEFAULT NULL,
  `version` varchar(5) DEFAULT NULL COMMENT 'tx version / tx 버젼 (v2, v3 )',
  `error_code` varchar(10) DEFAULT NULL,
  `error_msg` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal_tx_count` int(11) DEFAULT '0',
  PRIMARY KEY (`tx_hash`),
  UNIQUE KEY `tx_hash` (`tx_hash`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Transaction / 트랜잭션 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_TRANSACTION_TOTAL 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_TRANSACTION_TOTAL` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `height` int(11) NOT NULL COMMENT '블록 높이',
  `tx_hash` varchar(68) NOT NULL COMMENT '트랜잭션 해시',
  `hash` varchar(68) DEFAULT NULL COMMENT '블록 해시',
  `create_date` timestamp NULL DEFAULT NULL COMMENT '생성시간 UTC기준',
  `from_addr` varchar(68) DEFAULT NULL COMMENT '송금자 주소',
  `to_addr` varchar(68) DEFAULT NULL COMMENT '수금자 주소',
  `fee` varchar(30) DEFAULT NULL COMMENT '세금',
  `amount` varchar(30) DEFAULT NULL COMMENT '금액',
  `state` int(11) DEFAULT NULL COMMENT '0:실패, 1:성공',
  `tx_type` int(11) DEFAULT '0' COMMENT '0:ico, 1:token, 2:call, 3:install, 4:update',
  `step_limit` varchar(32) DEFAULT NULL,
  `step_used_tx` varchar(32) DEFAULT NULL,
  `step_price` varchar(32) DEFAULT NULL,
  `data_type` varchar(10) DEFAULT NULL,
  `version` varchar(5) DEFAULT NULL,
  `error_code` varchar(10) DEFAULT NULL,
  `error_msg` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal_tx_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`height`,`tx_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
/*!50100 PARTITION BY RANGE (height)
(PARTITION p00 VALUES LESS THAN (1000000) ENGINE = InnoDB,
 PARTITION p01 VALUES LESS THAN (2000000) ENGINE = InnoDB,
 PARTITION p02 VALUES LESS THAN (3000000) ENGINE = InnoDB,
 PARTITION p03 VALUES LESS THAN (4000000) ENGINE = InnoDB,
 PARTITION p04 VALUES LESS THAN (5000000) ENGINE = InnoDB,
 PARTITION p05 VALUES LESS THAN (6000000) ENGINE = InnoDB,
 PARTITION p06 VALUES LESS THAN (7000000) ENGINE = InnoDB,
 PARTITION p07 VALUES LESS THAN (8000000) ENGINE = InnoDB,
 PARTITION p08 VALUES LESS THAN (9000000) ENGINE = InnoDB,
 PARTITION p09 VALUES LESS THAN (10000000) ENGINE = InnoDB,
 PARTITION p10 VALUES LESS THAN (11000000) ENGINE = InnoDB,
 PARTITION p11 VALUES LESS THAN (12000000) ENGINE = InnoDB,
 PARTITION p12 VALUES LESS THAN (13000000) ENGINE = InnoDB,
 PARTITION p13 VALUES LESS THAN (14000000) ENGINE = InnoDB,
 PARTITION p14 VALUES LESS THAN (15000000) ENGINE = InnoDB,
 PARTITION p15 VALUES LESS THAN (16000000) ENGINE = InnoDB,
 PARTITION p16 VALUES LESS THAN (17000000) ENGINE = InnoDB,
 PARTITION p17 VALUES LESS THAN (18000000) ENGINE = InnoDB,
 PARTITION p18 VALUES LESS THAN (19000000) ENGINE = InnoDB,
 PARTITION p19 VALUES LESS THAN (20000000) ENGINE = InnoDB,
 PARTITION p20 VALUES LESS THAN (21000000) ENGINE = InnoDB,
 PARTITION p21 VALUES LESS THAN (22000000) ENGINE = InnoDB,
 PARTITION p22 VALUES LESS THAN (23000000) ENGINE = InnoDB,
 PARTITION p23 VALUES LESS THAN (24000000) ENGINE = InnoDB,
 PARTITION p24 VALUES LESS THAN (25000000) ENGINE = InnoDB,
 PARTITION p25 VALUES LESS THAN (26000000) ENGINE = InnoDB,
 PARTITION p26 VALUES LESS THAN (27000000) ENGINE = InnoDB,
 PARTITION p27 VALUES LESS THAN (28000000) ENGINE = InnoDB,
 PARTITION p28 VALUES LESS THAN (29000000) ENGINE = InnoDB,
 PARTITION p29 VALUES LESS THAN (30000000) ENGINE = InnoDB,
 PARTITION p30 VALUES LESS THAN (31000000) ENGINE = InnoDB,
 PARTITION p31 VALUES LESS THAN (32000000) ENGINE = InnoDB,
 PARTITION p32 VALUES LESS THAN (33000000) ENGINE = InnoDB,
 PARTITION p33 VALUES LESS THAN (34000000) ENGINE = InnoDB,
 PARTITION p34 VALUES LESS THAN (35000000) ENGINE = InnoDB,
 PARTITION p35 VALUES LESS THAN (36000000) ENGINE = InnoDB,
 PARTITION p36 VALUES LESS THAN (37000000) ENGINE = InnoDB,
 PARTITION p37 VALUES LESS THAN (38000000) ENGINE = InnoDB,
 PARTITION p38 VALUES LESS THAN (39000000) ENGINE = InnoDB,
 PARTITION p39 VALUES LESS THAN (40000000) ENGINE = InnoDB,
 PARTITION p40 VALUES LESS THAN (41000000) ENGINE = InnoDB,
 PARTITION p41 VALUES LESS THAN (42000000) ENGINE = InnoDB,
 PARTITION p42 VALUES LESS THAN (43000000) ENGINE = InnoDB,
 PARTITION p43 VALUES LESS THAN (44000000) ENGINE = InnoDB,
 PARTITION p44 VALUES LESS THAN (45000000) ENGINE = InnoDB,
 PARTITION p45 VALUES LESS THAN (46000000) ENGINE = InnoDB,
 PARTITION p46 VALUES LESS THAN (47000000) ENGINE = InnoDB,
 PARTITION p47 VALUES LESS THAN (48000000) ENGINE = InnoDB,
 PARTITION p48 VALUES LESS THAN (49000000) ENGINE = InnoDB,
 PARTITION p49 VALUES LESS THAN (50000000) ENGINE = InnoDB,
 PARTITION p50 VALUES LESS THAN (51000000) ENGINE = InnoDB,
 PARTITION p51 VALUES LESS THAN (52000000) ENGINE = InnoDB,
 PARTITION p52 VALUES LESS THAN (53000000) ENGINE = InnoDB,
 PARTITION p53 VALUES LESS THAN (54000000) ENGINE = InnoDB,
 PARTITION p54 VALUES LESS THAN (55000000) ENGINE = InnoDB,
 PARTITION p55 VALUES LESS THAN (56000000) ENGINE = InnoDB,
 PARTITION p56 VALUES LESS THAN (57000000) ENGINE = InnoDB,
 PARTITION p57 VALUES LESS THAN (58000000) ENGINE = InnoDB,
 PARTITION p58 VALUES LESS THAN (59000000) ENGINE = InnoDB,
 PARTITION p59 VALUES LESS THAN (60000000) ENGINE = InnoDB,
 PARTITION p60 VALUES LESS THAN (61000000) ENGINE = InnoDB,
 PARTITION p61 VALUES LESS THAN (62000000) ENGINE = InnoDB,
 PARTITION p62 VALUES LESS THAN (63000000) ENGINE = InnoDB,
 PARTITION p63 VALUES LESS THAN (64000000) ENGINE = InnoDB,
 PARTITION p64 VALUES LESS THAN (65000000) ENGINE = InnoDB,
 PARTITION p65 VALUES LESS THAN (66000000) ENGINE = InnoDB,
 PARTITION p66 VALUES LESS THAN (67000000) ENGINE = InnoDB,
 PARTITION p67 VALUES LESS THAN (68000000) ENGINE = InnoDB,
 PARTITION p68 VALUES LESS THAN (69000000) ENGINE = InnoDB,
 PARTITION p69 VALUES LESS THAN (70000000) ENGINE = InnoDB,
 PARTITION p70 VALUES LESS THAN (71000000) ENGINE = InnoDB,
 PARTITION p71 VALUES LESS THAN (72000000) ENGINE = InnoDB,
 PARTITION p72 VALUES LESS THAN (73000000) ENGINE = InnoDB,
 PARTITION p73 VALUES LESS THAN (74000000) ENGINE = InnoDB,
 PARTITION p74 VALUES LESS THAN (75000000) ENGINE = InnoDB,
 PARTITION p75 VALUES LESS THAN (76000000) ENGINE = InnoDB,
 PARTITION p76 VALUES LESS THAN (77000000) ENGINE = InnoDB,
 PARTITION p77 VALUES LESS THAN (78000000) ENGINE = InnoDB,
 PARTITION p78 VALUES LESS THAN (79000000) ENGINE = InnoDB,
 PARTITION p79 VALUES LESS THAN (80000000) ENGINE = InnoDB,
 PARTITION p80 VALUES LESS THAN (81000000) ENGINE = InnoDB,
 PARTITION p81 VALUES LESS THAN (82000000) ENGINE = InnoDB,
 PARTITION p82 VALUES LESS THAN (83000000) ENGINE = InnoDB,
 PARTITION p83 VALUES LESS THAN (84000000) ENGINE = InnoDB,
 PARTITION p84 VALUES LESS THAN (85000000) ENGINE = InnoDB,
 PARTITION p85 VALUES LESS THAN (86000000) ENGINE = InnoDB,
 PARTITION p86 VALUES LESS THAN (87000000) ENGINE = InnoDB,
 PARTITION p87 VALUES LESS THAN (88000000) ENGINE = InnoDB,
 PARTITION p88 VALUES LESS THAN (89000000) ENGINE = InnoDB,
 PARTITION p89 VALUES LESS THAN (90000000) ENGINE = InnoDB,
 PARTITION p90 VALUES LESS THAN (91000000) ENGINE = InnoDB,
 PARTITION p91 VALUES LESS THAN (92000000) ENGINE = InnoDB,
 PARTITION p92 VALUES LESS THAN (93000000) ENGINE = InnoDB,
 PARTITION p93 VALUES LESS THAN (94000000) ENGINE = InnoDB,
 PARTITION p94 VALUES LESS THAN (95000000) ENGINE = InnoDB,
 PARTITION p95 VALUES LESS THAN (96000000) ENGINE = InnoDB,
 PARTITION p96 VALUES LESS THAN (97000000) ENGINE = InnoDB,
 PARTITION p97 VALUES LESS THAN (98000000) ENGINE = InnoDB,
 PARTITION p98 VALUES LESS THAN (99000000) ENGINE = InnoDB,
 PARTITION p99 VALUES LESS THAN (100000000) ENGINE = InnoDB,
 PARTITION p100 VALUES LESS THAN MAXVALUE ENGINE = InnoDB) */;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_TRANSACTION_VIEW 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_TRANSACTION_VIEW` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_hash` varchar(68) COLLATE utf8_bin NOT NULL,
  `address` varchar(68) COLLATE utf8_bin NOT NULL,
  `height` int(11) NOT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `tx_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tx_hash` (`tx_hash`),
  KEY `address` (`address`),
  KEY `height` (`height`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_TX_DATA 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_TX_DATA` (
  `tx_hash` varchar(68) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data_body` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`tx_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Transaction data / 트랜잭션 데이터 ';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 icon_dev.T_TX_RESULT_LOG 구조 내보내기
CREATE TABLE IF NOT EXISTS `T_TX_RESULT_LOG` (
  `tx_hash` varchar(68) COLLATE utf8_bin NOT NULL,
  `tx_index` int(11) unsigned NOT NULL DEFAULT '0',
  `contract_addr` varchar(68) COLLATE utf8_bin DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `age` timestamp NULL DEFAULT NULL,
  `method` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `event_log` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `e_index` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`e_index`),
  KEY `contract_addr` (`contract_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 트리거 icon_dev.T_SCHEDULER_FLAG_BEFORE_INSERT 구조 내보내기
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ALLOW_INVALID_DATES';
DELIMITER //
CREATE TRIGGER `T_SCHEDULER_FLAG_BEFORE_INSERT` BEFORE INSERT ON `T_SCHEDULER_FLAG` FOR EACH ROW BEGIN

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- 트리거 icon_dev.T_SCHEDULER_FLAG_BEFORE_UPDATE 구조 내보내기
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ALLOW_INVALID_DATES';
DELIMITER //
CREATE TRIGGER `T_SCHEDULER_FLAG_BEFORE_UPDATE` BEFORE UPDATE ON `T_SCHEDULER_FLAG` FOR EACH ROW BEGIN
    set new.update_date = now();   
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
