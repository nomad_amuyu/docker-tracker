# Tracker-Docker
ICON supports Tracker-Docker for 3rd party or user services development. 


## _env File Setting
Depending on your server environment, you need to modify the _env file.

| PARAM | VALUE |
|:-----|:-----:|
| MYSQL_PATH | Mysql Folder  |
| APP_PATH | Application Folder  |
| TRACKER_URL | Tracker URL |
| MYSQL_DB | MySql Schema Name |
| MYSQL_USER | MySql User |
| MYSQL_PASS | Mysql  Password |
| MYSQL_ROOT_PASS | Mysql Root Password |
| PEER_IPADDR | Loop Chain Address |
| PEER_TYPE | Loop Chain Protocol |
| JAVA_SRC_PATH | jar File path (no modify) |
| SCORE_LOCAL_PATH | SCORE File path (no modify) |
| MYSQL_LOCAL_PATH | MySql Local path (no modify) |
| WEB_PORT | Web Server Port |
| FRONT_PORT | Front(React) Port |
| WAS_PORT | Was Server Port |
| MYSQL_PORT | MySql Port |

## Rename env File 
```
mv _env .env
```

## Docker Start
```
docker-compuse up -d 
```

## License

This project follows the Apache 2.0 License. Please refer to [LICENSE](https://www.apache.org/licenses/LICENSE-2.0) for details.